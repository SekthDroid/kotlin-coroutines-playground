package com.sekthdroid.coroutinesplayground.data

class ItemService {
    fun doSomethingIntensive(item: Item): Boolean {
        Thread.sleep(2000)

        return true
    }
}