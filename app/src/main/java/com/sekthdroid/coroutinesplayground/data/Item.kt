package com.sekthdroid.coroutinesplayground.data

data class Item(val id: String, val name: String)