package com.sekthdroid.coroutinesplayground.data

class ItemsRepository {
    fun all(): List<Item> {
        Thread.sleep(2000)
        return (0..100).map { Item(it.toString(), "Item $it") }
    }
}