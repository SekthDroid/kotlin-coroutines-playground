package com.sekthdroid.coroutinesplayground.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sekthdroid.coroutinesplayground.R
import com.sekthdroid.coroutinesplayground.data.Item
import kotlin.properties.Delegates

class ItemsAdapter(private val block: (Item) -> Unit) :
    RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {
    var items: List<Item> by Delegates.observable(emptyList()) { _, oldValue, newValue ->
        notifyItemRangeRemoved(0, oldValue.size)
        notifyItemRangeInserted(0, newValue.size)
    }

    class ItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val id: TextView = itemView.findViewById(R.id.idView)
        val name: TextView = itemView.findViewById(R.id.nameView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        return ItemsViewHolder(parent.inflate(R.layout.item_layout))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        with(items[position]) {
            holder.id.text = holder.itemView.getString(R.string.id_placeholder).plus(" ").plus(id)
            holder.name.text = name
            holder.itemView.setOnClickListener { block(this) }
        }
    }

    private fun View.getString(resId: Int) = context.getString(resId)

    private fun ViewGroup.inflate(layoutId: Int, attach: Boolean = false) =
        LayoutInflater.from(context).inflate(layoutId, this, attach)
}