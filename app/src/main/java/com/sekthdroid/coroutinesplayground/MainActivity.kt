package com.sekthdroid.coroutinesplayground

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.sekthdroid.coroutinesplayground.data.Item
import com.sekthdroid.coroutinesplayground.data.ItemService
import com.sekthdroid.coroutinesplayground.data.ItemsRepository
import com.sekthdroid.coroutinesplayground.ui.ItemsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    private var job: Job = Job()
    private var scope: CoroutineScope = CoroutineScope(Dispatchers.Main + job)

    private val repository: ItemsRepository by lazy { ItemsRepository() }
    private val itemService: ItemService by lazy { ItemService() }

    private val itemsAdapter: ItemsAdapter by lazy {
        ItemsAdapter(this::onItemClicked)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(itemsView) {
            adapter = itemsAdapter
            layoutManager = layoutManager()
            addItemDecoration(itemDecoration())
        }

        scope.launch {
            val data = fetch()
            itemsAdapter.items = data
        }
    }

    private fun onItemClicked(item: Item) {
        scope.launch {
            val result = withContext(Dispatchers.IO) { itemService.doSomethingIntensive(item) }

            startActivity(Intent(this@MainActivity, SecondActivity::class.java))
        }
    }

    private suspend fun fetch() = withContext(Dispatchers.IO) { repository.all() }

    private fun itemDecoration() = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

    private fun layoutManager() = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    override fun onPause() {
        super.onPause()
        job.cancelChildren()
    }
}
